﻿interface ITouch {
    identifier: string;
    screenX: number;
    screenY: number;
    clientX: number;
    clientY: number;
    pageX: number;
    pageY: number;
}

interface TouchEvent extends UIEvent {
    touches: ITouch[];
    targetTouches: number;
    changedTouches: ITouch[];
    //for ie
    pageX: number;
    pageY: number;
} 