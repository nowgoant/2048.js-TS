﻿interface IMetadata {
    score: number;
    bestScore: number;
    terminated: boolean;
    over: boolean;
    won: boolean;
}

export =IMetadata;