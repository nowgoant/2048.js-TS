﻿interface ITileSerialize {
    position: ITilePostion;
    value: number;
}

interface ITilePostion {
    x: number;
    y: number;
}

class Tile {
    x: number;
    y: number;
    value: number;
    mergedFrom: Tile[];
    previousPosition: ITilePostion;
    constructor(postions: ITilePostion, value: number) {
        this.x = postions.x;
        this.y = postions.y;
        this.value = value || 2;

        this.previousPosition = null;
        this.mergedFrom = null;
    }
    savePosition() {
        this.previousPosition = { x: this.x, y: this.y };
    }
    updatePosition(position: ITilePostion) {
        this.x = position.x;
        this.y = position.y;     
    }
    serialize(): ITileSerialize {
        return {
            position: {
                x: this.x,
                y: this.y
            },
            value: this.value
        };
    }
}